import React, { useEffect, useState } from "react";
import "./App.css";
import Component from "./component";

function App() {
  const [names, setNames] = useState("no name");
  // const getDatas = () => {
  //   fetch("http://localhost:3002/users").then((res) => console.log(res));
  // };

  // useEffect(() => {
  //   getDatas();
  // }, []);

  // const [inputs, setInputs] = useState({
  //   nama: "yogie",
  //   city: "Bandung",
  // });

  // const changeInput = (e) => {
  //   setInputs({
  //     ...inputs,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  return (
    //   <div className="container">
    //     <p>{inputs.nama}</p>
    //     <p>{inputs.city}</p>
    //     <input name="nama" onChange={changeInput}></input>
    //     <input name="city" onChange={changeInput}></input>
    //     <div className="flex">
    //       <div className="sidebar">
    //         <h1>Sidebar</h1>
    //         <p>Menu</p>
    //         <p>User</p>
    //         <p>Profile</p>
    //       </div>
    //       <div className="content">
    //         <h1>Black</h1>
    //         <div className="content-div">
    //           <h1>Content</h1>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    <div>
      <h1>React</h1>
      <Component gantiNama={setNames} />
      <p>{names}</p>
    </div>
  );
}

export default App;
