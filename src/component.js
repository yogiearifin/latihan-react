import React from "react";

const Component = ({ ...props }) => {
  console.log(props);
  const changeName = (e) => {
    props.gantiNama(e.target.value);
  };
  const value = "3";
  const nilai = 2;
  const arr = [
    {
      weapons: "WGS",
      type: "Claymore",
      atk: 600,
      rarity: 5,
    },
    {
      weapons: "Lost Prayer",
      type: "Catalyst",
      atk: 500,
      rarity: 5,
    },
    {
      weapons: "Skyward Harp",
      type: "Bow",
      atk: 550,
      rarity: 5,
    },
    {
      weapons: "Prototype Arhaic",
      type: "Claymore",
      atk: 450,
      rarity: 4,
    },
    {
      weapons: "Harbinger of Dawn",
      type: "Sword",
      atk: 400,
      rarity: 3,
    },
  ];

  const filterClaymore = () => {
    const fil = arr.filter((types) => types.type !== "Claymore");
    return fil;
  };

  console.log(filterClaymore());
  return (
    <>
      <div>
        <h2>I am a Component</h2>
        <p>{parseInt(value) + nilai}</p>
        {filterClaymore().map((item) => {
          return <p>{item.weapons}</p>;
        })}
      </div>
      <div>
        <input type="text" onChange={changeName}></input>
      </div>
    </>
  );
};
export default Component;
